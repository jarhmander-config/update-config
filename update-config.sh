#!/bin/bash

declare -a repo_list
let ret=0

for f in *; do
    if [[ -d "$f"/.git ]]; then
        echo ">> "$f"..."
        git -C "$f" pull --ff-only
        if [[ $? != 0 ]]; then
            let ret=1
            repo_list+=("$f")
        else
            SSH_AUTH_SOCK=/run/user/$(id -u)/keyring/ssh git -C "$f" push
        fi
        echo
    fi
done

if [[ $ret != 0 ]]; then
    echo ">>>"
    echo ">>> These repos needs attention:"
    echo ">>>"
    echo ">>> ${repo_list[@]}"
    echo ">>>"
    exit 1
fi
