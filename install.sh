#!/bin/bash
mkdir -p ~/.config/systemd/user
ln -sir update-config.service ~/.config/systemd/user
ln -sir update-config.timer   ~/.config/systemd/user
systemctl --user daemon-reload
systemctl --user enable --now update-config.timer
